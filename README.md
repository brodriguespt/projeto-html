## Projeto HTML
https://brodriguespt.gitlab.io/projeto-html/

Para fazer host de HTML no gitlab:
*  Copiar o ficheiro .gitlab-ci.yml deste projeto
*  Ir a **CI / CD > Pipelines**
*  Clicar em **Run Pipeline**
*  Ir a **Settings > Pages**
*  Link tem que aparecer em **Access Pages**